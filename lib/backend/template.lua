local backend = {}

local templates = {}
local environment = nil

function backend.template(t)
    table.insert(templates, t)
end

function backend.init(env)
    getmetatable(env).__index.template = backend.template
    environment = env
end

local function gen_template()
    local s = ""
    for i = 1, #templates do
        s = s .. templates[i]:gsub("%%(%b{})", function(expr)
            expr = expr:gsub("^{", ""):gsub("}$", "")
            local value = assert(load("return "..expr, expr, "t", environment))()
            return (value ~= nil) and tostring(value) or "${"..expr.."}"
        end)
    end
    return s
end

function backend.compile(output, ast, namespace)
    local ext = output:match"%.([^.]+)$" or ""
    return {
        {ext, gen_template()},
    }
end

return backend
