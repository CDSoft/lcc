local parser = {}

-- metatable for the configuration script execution
local envmt = {}
parser.envmt = envmt

local utils = require "utils"

-- always sort table keys (variables must always be generated in the same order)
local pairs = utils.pairs

-- populate the script environment with standard Lua functions and module
envmt.__index = {
    assert = assert,
    error = error,
    ipairs = ipairs,
    pairs = pairs, -- always sorted
    print = print,
    select = select,
    tonumber = tonumber,
    tostring = tostring,
    type = type,
    string = string,
    table = table,
    io = io,
    math = math,
    os = os,
}

local deps = {}

local import_dirs = {}

function parser.add_import_dir(dir)
    table.insert(import_dirs, dir)
end

do
    local stack = {}

    -- import a new script and execute it in the same environment than the caller
    function parser.import(name, env)
        -- same environment than the caller
        local caller = #stack > 0 and stack[#stack].caller
        env = env or stack[#stack].env
        local names = {}
        -- relative path starts from the path of the caller
        if name[0] ~= "/" and caller then
            table.insert(names, utils.dirname(caller).."/"..name)
        end
        -- or is relative to an import dir
        if name[0] ~= "/" then
            for _, import_dir in pairs(import_dirs) do
                table.insert(names, import_dir.."/"..name)
            end
        end
        -- or is relative to the lcc lib directory
        if name[0] ~= "/" then
            local libpath = arg[0]:gsub("/[^/]*$", "").."/lib"
            table.insert(names, libpath.."/"..name)
        end
        -- or is relative to the current working directory
        -- (only for command line parameters)
        -- or is absolute
        if not caller or name[0] == "/" then
            table.insert(names, name)
        end
        local found = false
        for _, name in ipairs(names) do
            if utils.file_exists(name) then
                table.insert(deps, name)
                stack[#stack+1] = {caller=name, env=env}
                assert(loadfile(name, "t", env))()
                stack[#stack] = nil
                found = true
                break
            end
        end
        assert(found, "File "..name.." not found")
    end
end
-- add the import function to the script environment metatable
envmt.__index.import = function(name) parser.import(name) end

local function is_scalar(t)
    return type(t) == "boolean" or type(t) == "number" or type(t) == "string"
end

local function is_array(t)
    if type(t) == "table" and not t.__custom then
        for _, _ in ipairs(t) do return true end
    end
end

local function is_struct(t)
    if type(t) == "table" and not t.__custom then
        for k, _ in pairs(t) do
            if type(k) == "string" then return true end
        end
    end
end

local function is_custom(t)
    return type(t) == "table" and t.__custom
end

local function is_hybrid(t)
    return is_array(t) and is_struct(t)
end

local function dump_path(path)
    local s = ""
    for _, comp in ipairs(path) do
        if type(comp) == "number" then s = s .. "[" .. comp .. "]"
        else s = s .. "." .. comp
        end
    end
    return s
end

local pathmt = {__tostring = dump_path}

local function add_path(path, comp)
    local path2 = utils.append(path, comp)
    return setmetatable(path2, pathmt)
end

local add_types, add_scalar_type, add_array_type, add_struct_type, add_custom_type

add_types = function(x, path, dim)
    path = path or setmetatable({}, {__tostring = dump_path})
    if is_hybrid(x) then error("Hybrid table: " .. tostring(path) .. " = " .. utils.dump(x))
    elseif is_array(x) then return add_array_type(x, path, dim)
    elseif is_struct(x) then return add_struct_type(x, path)
    elseif is_scalar(x) then return add_scalar_type(x, path)
    elseif is_custom(x) then return add_custom_type(x)
    else error("Invalid parameter: " .. tostring(path) .. " = " .. utils.dump(x))
    end
end

add_scalar_type = function(x, path)
    if type(x) == "boolean" then return {kind="bool"}
    elseif type(x) == "number" then
        if math.type(x) == "integer" then
            if 0 <= x and x < 1<<8 then return {kind="uint", size=8}
            elseif -(1<<7) <= x and x < 1<<7 then return {kind="int", size=8}
            elseif 0 <= x and x < 1<<16 then return {kind="uint", size=16}
            elseif -(1<<15) <= x and x < 1<<15 then return {kind="int", size=16}
            elseif 0 <= x and x < 1<<32 then return {kind="uint", size=32}
            elseif -(1<<31) <= x and x < 1<<31 then return {kind="int", size=32}
            elseif 0 <= x then return {kind="uint", size=64}
            else return {kind="int", size=64}
            end
        else return {kind="double"}
        end
    elseif type(x) == "string" then return {kind="str", size=#x}
    else error("Unknown scala type: " .. tostring(path) .. " = " .. utils.dump(x))
    end
end

add_custom_type = function(x)
    return x.__type
end

local merge_types, merge_array_types, merge_struct_types

merge_types = function(t1, t2, path)
    if not t1 then return t2 end
    if not t2 then return t1 end
    if t1.kind == "bool" and t2.kind == "bool" then return t1 end
    if t1.kind == "uint" and t2.kind == "uint" then return {kind="uint", size=math.max(t1.size, t2.size)} end
    if t1.kind == "int" and t2.kind == "int" then return {kind="int", size=math.max(t1.size, t2.size)} end
    if t1.kind == "uint" and t2.kind == "int" then return {kind="int", size=math.max(t1.size, t2.size)} end
    if t1.kind == "int" and t2.kind == "uint" then return {kind="int", size=math.max(t1.size, t2.size)} end
    if t1.kind == "double" and t2.kind == "double" then return t1 end
    if t1.kind == "str" and t2.kind == "str" then return {kind="str", size=math.max(t1.size, t2.size)} end
    if t1.kind == "array" and t2.kind == "array" then return merge_array_types(t1, t2, path) end
    if t1.kind == "struct" and t2.kind == "struct" then return merge_struct_types(t1, t2) end
    error("Can not merge types in " .. tostring(path))
end

merge_array_types = function(t1, t2, path)
    return {kind="array", size=math.max(t1.size, t2.size), itemtype=merge_types(t1.itemtype, t2.itemtype, path), dim=t1.dim}
end

merge_struct_types = function(t1, t2)
    local t = {kind="struct", fields={}}
    for k, v in pairs(t1.fields) do t.fields[k] = merge_types(t.fields[k], v) end
    for k, v in pairs(t2.fields) do t.fields[k] = merge_types(t.fields[k], v) end
    return t
end

add_array_type = function(x, path, dim)
    -- all items have the same type
    dim = (dim or 0) + 1
    x.__type = {kind="array", size=#x, itemtype=nil, dim=dim}
    for i, v in ipairs(x) do
        local path2 = add_path(path, i)
        local itemtype = add_types(v, path2, dim)
        x.__type.itemtype = merge_types(x.__type.itemtype, itemtype, path2)
    end
    return x.__type
end

local function filter(k, v)
    if k:match"^__" then return false end
    if type(v) == "function" then return false end
    return true
end

add_struct_type = function(x, path)
    -- each field has its own type
    x.__type = {kind="struct", fields={}}
    for k, v in pairs(x) do
        if filter(k, v) then
            local path2 = add_path(path, k)
            local field_type = add_types(v, path2)
            x.__type.fields[k] = field_type
        end
    end
    return x.__type
end

local function custom(definitions)
    return function(name)
        return {
            __custom = true,
            __type = { kind = "custom", definitions = definitions },
            name = name
        }
    end
end

envmt.__index["custom"] = custom

local __prelude = {}

local function prelude(backend)
    return function(code)
        local t = __prelude[backend] or {}
        table.insert(t, code)
        __prelude[backend] = t
    end
end

envmt.__index["prelude"] = prelude
envmt.__index["__prelude"] = __prelude

function parser.compile(env)
    add_types(env)
    return env
end

do
    local checkers = {}
    function envmt.__index.check(f)
        table.insert(checkers, f)
    end
    function parser.check()
        for i = 1, #checkers do
            checkers[i]()
        end
    end
end

function parser.leaves(x, f, path, t)
    path = path or setmetatable({}, pathmt)
    if type(x) == "table" and x.__type.kind == "array" then
        for i, v in ipairs(x) do
            local path2 = add_path(path, i)
            parser.leaves(v, f, path2, x.__type.itemtype)
        end
    elseif type(x) == "table" and x.__type.kind == "struct" then
        for k, v in pairs(x) do
            if filter(k, v) then
                local path2 = add_path(path, k)
                parser.leaves(v, f, path2, x.__type.fields[k])
            end
        end
    elseif type(x) == "table" and x.__type.kind == "custom" then
        f(path, x, t)
    elseif type(x) ~= "table" then
        f(path, x, t)
    end
end

function parser.save_dependencies(depfile, targets)
    io.open(depfile, "w"):write(("%s: %s\n"):format(
        table.concat(targets, " "),
        table.concat(deps, " ")
    ))
end

return parser
