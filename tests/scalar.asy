struct t_conf_boolean {
    bool f;
    bool t;
}
struct t_conf_floats {
    real f1;
    real f2;
    real f3;
    real f4;
}
struct t_conf_neg {
    int a;
    int b;
    int c;
    int d;
}
struct t_conf_pos {
    int a;
    int b;
    int c;
    int d;
    int e;
    int f;
    int g;
    int h;
}
struct t_conf_strings {
    string s1;
    string s2;
    string s3;
}
struct t_conf {
    int a;
    int b;
    int b1;
    int b2;
    int b3;
    int b4;
    t_conf_boolean boolean;
    t_conf_floats floats;
    t_conf_neg neg;
    t_conf_pos pos;
    t_conf_strings strings;
}
t_conf conf;
conf.a = 9223372036854775807;
conf.b = -9223372036854775804-4;
conf.b1 = -9223372036854775804-3;
conf.b2 = -9223372036854775804-2;
conf.b3 = -9223372036854775804-1;
conf.b4 = -9223372036854775804;
conf.boolean.f = false;
conf.boolean.t = true;
conf.floats.f1 = 0.0;
conf.floats.f2 = 1.0;
conf.floats.f3 = 3.1;
conf.floats.f4 = 3.1415926535898;
conf.neg.a = -128;
conf.neg.b = -32768;
conf.neg.c = -2147483648;
conf.neg.d = -9223372036854775804-4;
conf.pos.a = 0;
conf.pos.b = 128;
conf.pos.c = 256;
conf.pos.d = 32768;
conf.pos.e = 65536;
conf.pos.f = 2147483648;
conf.pos.g = 4294967296;
conf.pos.h = 8589934592;
conf.strings.s1 = "";
conf.strings.s2 = "hello";
conf.strings.s3 = "hello world!";

