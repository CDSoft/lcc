module Checker
where
data ConfBoundingBox = ConfBoundingBox
    { boundingBoxXmax' :: Double
    , boundingBoxXmin' :: Double
    , boundingBoxYmax' :: Double
    , boundingBoxYmin' :: Double
    }
data ConfPoints = ConfPoints
    { pointsX' :: Double
    , pointsY' :: Double
    }
data Conf = Conf
    { boundingBox' :: ConfBoundingBox
    , points' :: [ConfPoints]
    }
conf :: Conf
conf = Conf
    { boundingBox' = ConfBoundingBox {boundingBoxXmax' = 12.0, boundingBoxXmin' = 6.0, boundingBoxYmax' = 14.0, boundingBoxYmin' = -2.0}
    , points' =
        [ ConfPoints {pointsX' = 12.0, pointsY' = 7.0}
        , ConfPoints {pointsX' = 6.0, pointsY' = 14.0}
        , ConfPoints {pointsX' = 8.0, pointsY' = -2.0}
        ]
    }
