s = {
    {x=1, y="one"},
    {x=2, y="two"},
}
if template then -- for the template backend only

    template [[
%{s[1].x} -> %{s[1].y}
]]

    template [[
%{s[2].x} -> %{s[2].y}
]]

    template [[
%{undefined}
]]

end
