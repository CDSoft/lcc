.. |CONF_A| replace:: 9223372036854775807

.. |CONF_B| replace:: -9223372036854775808

.. |CONF_B1| replace:: -9223372036854775807

.. |CONF_B2| replace:: -9223372036854775806

.. |CONF_B3| replace:: -9223372036854775805

.. |CONF_B4| replace:: -9223372036854775804

.. |CONF_BOOLEAN_F| replace:: false

.. |CONF_BOOLEAN_T| replace:: true

.. |CONF_FLOATS_F1| replace:: 0.0

.. |CONF_FLOATS_F2| replace:: 1.0

.. |CONF_FLOATS_F3| replace:: 3.1

.. |CONF_FLOATS_F4| replace:: 3.1415926535898

.. |CONF_NEG_A| replace:: -128

.. |CONF_NEG_B| replace:: -32768

.. |CONF_NEG_C| replace:: -2147483648

.. |CONF_NEG_D| replace:: -9223372036854775808

.. |CONF_POS_A| replace:: 0

.. |CONF_POS_B| replace:: 128

.. |CONF_POS_C| replace:: 256

.. |CONF_POS_D| replace:: 32768

.. |CONF_POS_E| replace:: 65536

.. |CONF_POS_F| replace:: 2147483648

.. |CONF_POS_G| replace:: 4294967296

.. |CONF_POS_H| replace:: 8589934592

.. |CONF_STRINGS_S1| replace:: 

.. |CONF_STRINGS_S2| replace:: hello

.. |CONF_STRINGS_S3| replace:: hello world!

