#pragma once
#include <stdbool.h>
#include <stdint.h>
enum t_state { INIT, RUNNING, DEAD };

int fib(int x);
#define CONF_F (fib)
#define CONF_HELLOSTR ("Hello World!")
#define CONF_INITIAL_STATE (INIT)
#define CONF_MY_PTR ((void*)0x00001234)
#define CONF_RUNNING (RUNNING)
struct t_conf
{
    int (*f)(int x);
    char *hellostr;
    enum t_state initial_state;
    void *my_ptr;
    enum t_state running;
};
extern const struct t_conf CONF;
