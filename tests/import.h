#pragma once
#include <stdbool.h>
#include <stdint.h>
#define CONF_IMPORTED_PARAMETER (42)
#define CONF_PARAMETER_FROM_IMPORTED_LIB (21)
struct t_conf
{
    uint8_t imported_parameter;
    uint8_t parameter_from_imported_lib;
};
extern const struct t_conf CONF;
