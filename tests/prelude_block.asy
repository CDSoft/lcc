/* This comment must be added to the Haskell file */
int INCREDIBLE_CONSTANT = 42;

/* Several sections can be added to the prelude */
struct t_conf_structured_answer {
    int answer;
    string question;
}
struct t_conf {
    int answer_to_everything;
    t_conf_structured_answer structured_answer;
}
t_conf conf;
conf.answer_to_everything = INCREDIBLE_CONSTANT;
conf.structured_answer.answer = INCREDIBLE_CONSTANT;
conf.structured_answer.question = "INCREDIBLE_CONSTANT";

