struct t_conf_bounding_box {
    real xmax;
    real xmin;
    real ymax;
    real ymin;
}
struct t_conf_points {
    real x;
    real y;
}
struct t_conf {
    t_conf_bounding_box bounding_box;
    t_conf_points []points;
}
t_conf conf;
conf.bounding_box.xmax = 12.0;
conf.bounding_box.xmin = 6.0;
conf.bounding_box.ymax = 14.0;
conf.bounding_box.ymin = -2.0;
conf.points[1] = new t_conf_points ;
conf.points[1].x = 12.0;
conf.points[1].y = 7.0;
conf.points[2] = new t_conf_points ;
conf.points[2].x = 6.0;
conf.points[2].y = 14.0;
conf.points[3] = new t_conf_points ;
conf.points[3].x = 8.0;
conf.points[3].y = -2.0;

