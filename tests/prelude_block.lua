prelude "c" [[
/* This comment must be added to the C header file */
#define INCREDIBLE_CONSTANT 42
]]
prelude "hs" [[
{- This comment must be added to the Haskell file -}
data INCREDIBLE_CONSTANT = INCREDIBLE_CONSTANT
]]
prelude "asy" [[
/* This comment must be added to the Haskell file */
int INCREDIBLE_CONSTANT = 42;
]]

prelude "c" [[
/* Several sections can be added to the prelude */
]]
prelude "hs" [[
{- Several sections can be added to the prelude -}
]]
prelude "asy" [[
/* Several sections can be added to the prelude */
]]

local cte = custom {
    c={t="int %s"},
    sh={v="%s"},
    hs={t="%s :: INCREDIBLE_CONSTANT", v="%s"},
    asy={t="int %s", v="%s"},
    yaml={v="%s"},
}

AnswerToEverything = cte "INCREDIBLE_CONSTANT"

StructuredAnswer = {
    Question = "INCREDIBLE_CONSTANT",
    Answer = cte "INCREDIBLE_CONSTANT",
}
