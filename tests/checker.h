#pragma once
#include <stdbool.h>
#include <stdint.h>
#define CONF_BOUNDING_BOX_XMAX (12.0)
#define CONF_BOUNDING_BOX_XMIN (6.0)
#define CONF_BOUNDING_BOX_YMAX (14.0)
#define CONF_BOUNDING_BOX_YMIN (-2.0)
#define CONF_POINTS_1_X (12.0)
#define CONF_POINTS_1_Y (7.0)
#define CONF_POINTS_2_X (6.0)
#define CONF_POINTS_2_Y (14.0)
#define CONF_POINTS_3_X (8.0)
#define CONF_POINTS_3_Y (-2.0)
struct t_conf
{
    struct t_conf_bounding_box
    {
        double xmax;
        double xmin;
        double ymax;
        double ymin;
    } bounding_box;
#define CONF_POINTS_SIZE1 3
    struct t_conf_points
    {
        double x;
        double y;
    } points[CONF_POINTS_SIZE1];
};
extern const struct t_conf CONF;
