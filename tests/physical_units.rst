.. |CONF_ANGLE1_DEG| replace:: 45

.. |CONF_ANGLE1_RAD| replace:: 0.78539816339745

.. |CONF_ANGLE2_DEG| replace:: -60.0

.. |CONF_ANGLE2_RAD| replace:: -1.0471975511966

.. |CONF_ARRAY_1_ANGLE1_DEG| replace:: 45

.. |CONF_ARRAY_1_ANGLE1_RAD| replace:: 0.78539816339745

.. |CONF_ARRAY_1_ANGLE2_DEG| replace:: -60.0

.. |CONF_ARRAY_1_ANGLE2_RAD| replace:: -1.0471975511966

.. |CONF_ARRAY_1_LENGTH1_CM| replace:: 520

.. |CONF_ARRAY_1_LENGTH1_M| replace:: 5.2

.. |CONF_ARRAY_1_LENGTH1_MM| replace:: 5200

.. |CONF_ARRAY_1_LENGTH2_CM| replace:: 5.2

.. |CONF_ARRAY_1_LENGTH2_M| replace:: 0.052

.. |CONF_ARRAY_1_LENGTH2_MM| replace:: 52

.. |CONF_ARRAY_1_LENGTH3_CM| replace:: 0.52

.. |CONF_ARRAY_1_LENGTH3_M| replace:: 0.0052

.. |CONF_ARRAY_1_LENGTH3_MM| replace:: 5.2

.. |CONF_ARRAY_1_SPEED1_KPH| replace:: 19.98

.. |CONF_ARRAY_1_SPEED1_MPS| replace:: 5.55

.. |CONF_ARRAY_1_SPEED2_KPH| replace:: 10

.. |CONF_ARRAY_1_SPEED2_MPS| replace:: 2.7777777777778

.. |CONF_ARRAY_1_TIME1_MS| replace:: 4100

.. |CONF_ARRAY_1_TIME1_S| replace:: 4.1

.. |CONF_ARRAY_1_TIME2_MS| replace:: 4.1

.. |CONF_ARRAY_1_TIME2_S| replace:: 0.0041

.. |CONF_LENGTH1_CM| replace:: 520

.. |CONF_LENGTH1_M| replace:: 5.2

.. |CONF_LENGTH1_MM| replace:: 5200

.. |CONF_LENGTH2_CM| replace:: 5.2

.. |CONF_LENGTH2_M| replace:: 0.052

.. |CONF_LENGTH2_MM| replace:: 52

.. |CONF_LENGTH3_CM| replace:: 0.52

.. |CONF_LENGTH3_M| replace:: 0.0052

.. |CONF_LENGTH3_MM| replace:: 5.2

.. |CONF_SPEED1_KPH| replace:: 19.98

.. |CONF_SPEED1_MPS| replace:: 5.55

.. |CONF_SPEED2_KPH| replace:: 10

.. |CONF_SPEED2_MPS| replace:: 2.7777777777778

.. |CONF_TIME1_MS| replace:: 4100

.. |CONF_TIME1_S| replace:: 4.1

.. |CONF_TIME2_MS| replace:: 4.1

.. |CONF_TIME2_S| replace:: 0.0041

