module PreludeBlock
where
{- This comment must be added to the Haskell file -}
data INCREDIBLE_CONSTANT = INCREDIBLE_CONSTANT

{- Several sections can be added to the prelude -}
data ConfStructuredAnswer = ConfStructuredAnswer
    { structuredAnswerAnswer' :: INCREDIBLE_CONSTANT
    , structuredAnswerQuestion' :: String
    }
data Conf = Conf
    { answerToEverything' :: INCREDIBLE_CONSTANT
    , structuredAnswer' :: ConfStructuredAnswer
    }
conf :: Conf
conf = Conf
    { answerToEverything' = INCREDIBLE_CONSTANT
    , structuredAnswer' = ConfStructuredAnswer {structuredAnswerAnswer' = INCREDIBLE_CONSTANT, structuredAnswerQuestion' = "INCREDIBLE_CONSTANT"}
    }
