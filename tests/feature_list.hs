module FeatureList
where
data Conf = Conf
    { hasBar' :: Bool
    , hasBaz' :: Bool
    , hasFoo' :: Bool
    }
conf :: Conf
conf = Conf
    { hasBar' = True
    , hasBaz' = True
    , hasFoo' = True
    }
