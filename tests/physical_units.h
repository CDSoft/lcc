#pragma once
#include <stdbool.h>
#include <stdint.h>
#define CONF_ANGLE1_DEG (45)
#define CONF_ANGLE1_RAD (0.78539816339745)
#define CONF_ANGLE2_DEG (-60.0)
#define CONF_ANGLE2_RAD (-1.0471975511966)
#define CONF_ARRAY_1_ANGLE1_DEG (45)
#define CONF_ARRAY_1_ANGLE1_RAD (0.78539816339745)
#define CONF_ARRAY_1_ANGLE2_DEG (-60.0)
#define CONF_ARRAY_1_ANGLE2_RAD (-1.0471975511966)
#define CONF_ARRAY_1_LENGTH1_CM (520)
#define CONF_ARRAY_1_LENGTH1_M (5.2)
#define CONF_ARRAY_1_LENGTH1_MM (5200)
#define CONF_ARRAY_1_LENGTH2_CM (5.2)
#define CONF_ARRAY_1_LENGTH2_M (0.052)
#define CONF_ARRAY_1_LENGTH2_MM (52)
#define CONF_ARRAY_1_LENGTH3_CM (0.52)
#define CONF_ARRAY_1_LENGTH3_M (0.0052)
#define CONF_ARRAY_1_LENGTH3_MM (5.2)
#define CONF_ARRAY_1_SPEED1_KPH (19.98)
#define CONF_ARRAY_1_SPEED1_MPS (5.55)
#define CONF_ARRAY_1_SPEED2_KPH (10)
#define CONF_ARRAY_1_SPEED2_MPS (2.7777777777778)
#define CONF_ARRAY_1_TIME1_MS (4100)
#define CONF_ARRAY_1_TIME1_S (4.1)
#define CONF_ARRAY_1_TIME2_MS (4.1)
#define CONF_ARRAY_1_TIME2_S (0.0041)
#define CONF_LENGTH1_CM (520)
#define CONF_LENGTH1_M (5.2)
#define CONF_LENGTH1_MM (5200)
#define CONF_LENGTH2_CM (5.2)
#define CONF_LENGTH2_M (0.052)
#define CONF_LENGTH2_MM (52)
#define CONF_LENGTH3_CM (0.52)
#define CONF_LENGTH3_M (0.0052)
#define CONF_LENGTH3_MM (5.2)
#define CONF_SPEED1_KPH (19.98)
#define CONF_SPEED1_MPS (5.55)
#define CONF_SPEED2_KPH (10)
#define CONF_SPEED2_MPS (2.7777777777778)
#define CONF_TIME1_MS (4100)
#define CONF_TIME1_S (4.1)
#define CONF_TIME2_MS (4.1)
#define CONF_TIME2_S (0.0041)
struct t_conf
{
    uint8_t angle1_deg;
    double angle1_rad;
    double angle2_deg;
    double angle2_rad;
#define CONF_ARRAY_SIZE1 1
    struct t_conf_array
    {
        uint8_t angle1_deg;
        double angle1_rad;
        double angle2_deg;
        double angle2_rad;
        uint16_t length1_cm;
        double length1_m;
        uint16_t length1_mm;
        double length2_cm;
        double length2_m;
        uint8_t length2_mm;
        double length3_cm;
        double length3_m;
        double length3_mm;
        double speed1_kph;
        double speed1_mps;
        uint8_t speed2_kph;
        double speed2_mps;
        uint16_t time1_ms;
        double time1_s;
        double time2_ms;
        double time2_s;
    } array[CONF_ARRAY_SIZE1];
    uint16_t length1_cm;
    double length1_m;
    uint16_t length1_mm;
    double length2_cm;
    double length2_m;
    uint8_t length2_mm;
    double length3_cm;
    double length3_m;
    double length3_mm;
    double speed1_kph;
    double speed1_mps;
    uint8_t speed2_kph;
    double speed2_mps;
    uint16_t time1_ms;
    double time1_s;
    double time2_ms;
    double time2_s;
};
extern const struct t_conf CONF;
