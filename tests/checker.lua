check(function()
    assert(points and #points > 1)
    assert(bounding_box == nil)
    local xmin = points[1].x
    local xmax = points[1].x
    local ymin = points[1].y
    local ymax = points[1].y
    for i = 2, #points do
        xmin = math.min(xmin, points[i].x)
        xmax = math.max(xmax, points[i].x)
        ymin = math.min(ymin, points[i].y)
        ymax = math.max(ymax, points[i].y)
    end
    bounding_box = {xmin=xmin, xmax=xmax, ymin=ymin, ymax=ymax}
end)

points = {
    {x=12.0, y=7.0},
    {x=6.0, y=14.0},
    {x=8.0, y=-2.0},
}
