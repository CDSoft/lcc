#pragma once
#include <stdbool.h>
#include <stdint.h>
#define CONF_A (9223372036854775807LLU)
#define CONF_B (-9223372036854775807LL-1LL)
#define CONF_B1 (-9223372036854775807LL)
#define CONF_B2 (-9223372036854775806LL)
#define CONF_B3 (-9223372036854775805LL)
#define CONF_B4 (-9223372036854775804LL)
#define CONF_BOOLEAN_F (false)
#define CONF_BOOLEAN_T (true)
#define CONF_FLOATS_F1 (0.0)
#define CONF_FLOATS_F2 (1.0)
#define CONF_FLOATS_F3 (3.1)
#define CONF_FLOATS_F4 (3.1415926535898)
#define CONF_NEG_A (-128)
#define CONF_NEG_B (-32768)
#define CONF_NEG_C (-2147483648)
#define CONF_NEG_D (-9223372036854775807LL-1LL)
#define CONF_POS_A (0)
#define CONF_POS_B (128)
#define CONF_POS_C (256)
#define CONF_POS_D (32768)
#define CONF_POS_E (65536)
#define CONF_POS_F (2147483648)
#define CONF_POS_G (4294967296LLU)
#define CONF_POS_H (8589934592LLU)
#define CONF_STRINGS_S1 ("")
#define CONF_STRINGS_S2 ("hello")
#define CONF_STRINGS_S3 ("hello world!")
struct t_conf
{
    uint64_t a;
    int64_t b;
    int64_t b1;
    int64_t b2;
    int64_t b3;
    int64_t b4;
    struct t_conf_boolean
    {
        bool f;
        bool t;
    } boolean;
    struct t_conf_floats
    {
        double f1;
        double f2;
        double f3;
        double f4;
    } floats;
    struct t_conf_neg
    {
        int8_t a;
        int16_t b;
        int32_t c;
        int64_t d;
    } neg;
    struct t_conf_pos
    {
        uint8_t a;
        uint8_t b;
        uint16_t c;
        uint16_t d;
        uint32_t e;
        uint32_t f;
        uint64_t g;
        uint64_t h;
    } pos;
    struct t_conf_strings
    {
#define CONF_STRINGS_S1_LEN 1
        char s1[CONF_STRINGS_S1_LEN];
#define CONF_STRINGS_S2_LEN 6
        char s2[CONF_STRINGS_S2_LEN];
#define CONF_STRINGS_S3_LEN 13
        char s3[CONF_STRINGS_S3_LEN];
    } strings;
};
extern const struct t_conf CONF;
