struct t_conf_flat_structure {
    int field1;
    string field2;
    bool field3;
    real field4;
}
struct t_conf_mega_struct_field_02 {
    int x;
    int y;
}
struct t_conf_mega_struct_field_03 {
    string a;
    bool b;
}
struct t_conf_mega_struct_field_06_st {
    int a;
    int b;
}
struct t_conf_mega_struct_field_06 {
    string []arr;
    string s;
    string s2;
    t_conf_mega_struct_field_06_st st;
    bool t;
    int x;
    string y;
    real z;
}
struct t_conf_mega_struct {
    int field_01;
    t_conf_mega_struct_field_02 field_02;
    t_conf_mega_struct_field_03 field_03;
    string []field_04;
    int [][]field_05;
    t_conf_mega_struct_field_06 [][][]field_06;
}
struct t_conf_table {
    string same_name;
}
struct t_conf {
    int []flat_array;
    t_conf_flat_structure flat_structure;
    t_conf_mega_struct mega_struct;
    string same_name;
    t_conf_table table;
}
t_conf conf;
conf.flat_array[1] = 10;
conf.flat_array[2] = 20;
conf.flat_array[3] = 30;
conf.flat_array[4] = 40;
conf.flat_structure.field1 = 42;
conf.flat_structure.field2 = "fourty two";
conf.flat_structure.field3 = true;
conf.flat_structure.field4 = 3.1415926535898;
conf.mega_struct.field_01 = 42;
conf.mega_struct.field_02.x = 0;
conf.mega_struct.field_02.y = 1;
conf.mega_struct.field_03.a = "hi";
conf.mega_struct.field_03.b = true;
conf.mega_struct.field_04[1] = "one";
conf.mega_struct.field_04[2] = "two";
conf.mega_struct.field_04[3] = "three";
conf.mega_struct.field_05[1] = new int [];
conf.mega_struct.field_05[1][1] = 1;
conf.mega_struct.field_05[1][2] = 2;
conf.mega_struct.field_05[2] = new int [];
conf.mega_struct.field_05[2][1] = 3;
conf.mega_struct.field_05[2][2] = 4;
conf.mega_struct.field_05[3] = new int [];
conf.mega_struct.field_05[3][1] = 5;
conf.mega_struct.field_05[3][2] = 6;
conf.mega_struct.field_06[1] = new t_conf_mega_struct_field_06 [][];
conf.mega_struct.field_06[1][1] = new t_conf_mega_struct_field_06 [];
conf.mega_struct.field_06[1][1][1] = new t_conf_mega_struct_field_06 ;
conf.mega_struct.field_06[1][1][1].st.a = 1;
conf.mega_struct.field_06[1][1][1].st.b = 2;
conf.mega_struct.field_06[1][1][1].x = 1;
conf.mega_struct.field_06[1][1][1].y = "one";
conf.mega_struct.field_06[1][1][2] = new t_conf_mega_struct_field_06 ;
conf.mega_struct.field_06[1][1][2].x = 2;
conf.mega_struct.field_06[1][1][2].y = "two";
conf.mega_struct.field_06[1][2] = new t_conf_mega_struct_field_06 [];
conf.mega_struct.field_06[1][2][1] = new t_conf_mega_struct_field_06 ;
conf.mega_struct.field_06[1][2][1].x = -1;
conf.mega_struct.field_06[1][2][2] = new t_conf_mega_struct_field_06 ;
conf.mega_struct.field_06[1][2][2].y = "N/A";
conf.mega_struct.field_06[1][3] = new t_conf_mega_struct_field_06 [];
conf.mega_struct.field_06[1][3][1] = new t_conf_mega_struct_field_06 ;
conf.mega_struct.field_06[1][3][1].st.a = 3;
conf.mega_struct.field_06[1][3][1].st.b = 4;
conf.mega_struct.field_06[1][3][1].z = 0.1;
conf.mega_struct.field_06[1][3][2] = new t_conf_mega_struct_field_06 ;
conf.mega_struct.field_06[1][3][2].z = 0.2;
conf.mega_struct.field_06[2] = new t_conf_mega_struct_field_06 [][];
conf.mega_struct.field_06[2][1] = new t_conf_mega_struct_field_06 [];
conf.mega_struct.field_06[2][1][1] = new t_conf_mega_struct_field_06 ;
conf.mega_struct.field_06[2][1][1].arr[1] = "a";
conf.mega_struct.field_06[2][1][1].arr[2] = "b";
conf.mega_struct.field_06[2][1][1].x = 10;
conf.mega_struct.field_06[2][1][1].y = "ten";
conf.mega_struct.field_06[2][1][2] = new t_conf_mega_struct_field_06 ;
conf.mega_struct.field_06[2][1][2].x = 20;
conf.mega_struct.field_06[2][1][2].y = "twenty";
conf.mega_struct.field_06[2][2] = new t_conf_mega_struct_field_06 [];
conf.mega_struct.field_06[2][2][1] = new t_conf_mega_struct_field_06 ;
conf.mega_struct.field_06[2][2][1].x = -2;
conf.mega_struct.field_06[2][2][2] = new t_conf_mega_struct_field_06 ;
conf.mega_struct.field_06[2][2][2].y = "N/A";
conf.mega_struct.field_06[2][3] = new t_conf_mega_struct_field_06 [];
conf.mega_struct.field_06[2][3][1] = new t_conf_mega_struct_field_06 ;
conf.mega_struct.field_06[2][3][1].arr[1] = "cd";
conf.mega_struct.field_06[2][3][1].arr[2] = "efg";
conf.mega_struct.field_06[2][3][1].z = 3.1415926535898;
conf.mega_struct.field_06[2][3][2] = new t_conf_mega_struct_field_06 ;
conf.mega_struct.field_06[2][3][2].z = 1.5707963267949;
conf.mega_struct.field_06[3] = new t_conf_mega_struct_field_06 [][];
conf.mega_struct.field_06[3][1] = new t_conf_mega_struct_field_06 [];
conf.mega_struct.field_06[3][1][1] = new t_conf_mega_struct_field_06 ;
conf.mega_struct.field_06[3][1][1].s = "";
conf.mega_struct.field_06[3][1][1].s2 = "---------------------------------------";
conf.mega_struct.field_06[4] = new t_conf_mega_struct_field_06 [][];
conf.mega_struct.field_06[4][1] = new t_conf_mega_struct_field_06 [];
conf.mega_struct.field_06[4][1][1] = new t_conf_mega_struct_field_06 ;
conf.mega_struct.field_06[4][1][1].t = true;
conf.same_name = "global";
conf.table.same_name = "field";

