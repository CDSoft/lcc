#pragma once
#include <stdbool.h>
#include <stdint.h>
/* This comment must be added to the C header file */
#define INCREDIBLE_CONSTANT 42

/* Several sections can be added to the prelude */
#define CONF_ANSWER_TO_EVERYTHING (INCREDIBLE_CONSTANT)
#define CONF_STRUCTURED_ANSWER_ANSWER (INCREDIBLE_CONSTANT)
#define CONF_STRUCTURED_ANSWER_QUESTION ("INCREDIBLE_CONSTANT")
struct t_conf
{
    int answer_to_everything;
    struct t_conf_structured_answer
    {
        int answer;
#define CONF_STRUCTURED_ANSWER_QUESTION_LEN 20
        char question[CONF_STRUCTURED_ANSWER_QUESTION_LEN];
    } structured_answer;
};
extern const struct t_conf CONF;
