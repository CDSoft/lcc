time1_s = 4.1
time2_ms = 4.1

length1_m = 5.2
length2_cm = 5.2
length3_mm = 5.2

angle1_deg = 45
angle2_rad = -math.pi/3

speed1_mps = 5.55
speed2_kph = 10

array = {
    {   time1_s = 4.1,
        time2_ms = 4.1,

        length1_m = 5.2,
        length2_cm = 5.2,
        length3_mm = 5.2,

        angle1_deg = 45,
        angle2_rad = -math.pi/3,

        speed1_mps = 5.55,
        speed2_kph = 10,
    }
}
