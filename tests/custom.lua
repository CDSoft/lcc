prelude "c" [[
enum t_state { INIT, RUNNING, DEAD };
]]

prelude "hs" [[
data State = INIT | RUNNING | DEAD
]]

local state = custom {
    c={t="enum t_state %s"},
    sh={v="%s"},
    hs={t="%s :: State"},
    asy={t="string %s", v=[["%s"]]},
    yaml={v=[["%s"]]},
}

initial_state = state "INIT"
running = state "RUNNING"

local ptr = custom {
    c={t="void *%s", v="(void*)0x%08X"},
    sh={v="0x%08X"},
    hs={t="%s :: Int", v="0x%08X"},
    asy={t="int %s", v="%d"},
    yaml={v="%d"},
}

my_ptr = ptr(0x1234)

prelude "c" [[
int fib(int x);
]]

local func = custom {
    c={t="int (*%s)(int x)"},
    sh={v="%s"},
    hs={t="%s :: String", v=[["%s"]]},
    asy={t="string %s", v=[["%s"]]},
    yaml={v=[["%s"]]},
}

f = func "fib"

-- custom types can be defined in a library
import "lib/custom_type.lua"

hellostr = say_hello "World!"
