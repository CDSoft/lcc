#include "physical_units.h"
const struct t_conf CONF = {
    .angle1_deg = 45,
    .angle1_rad = 0.78539816339745,
    .angle2_deg = -60.0,
    .angle2_rad = -1.0471975511966,
    .array = {
        [0] = {.angle1_deg = 45, .angle1_rad = 0.78539816339745, .angle2_deg = -60.0, .angle2_rad = -1.0471975511966, .length1_cm = 520, .length1_m = 5.2, .length1_mm = 5200, .length2_cm = 5.2, .length2_m = 0.052, .length2_mm = 52, .length3_cm = 0.52, .length3_m = 0.0052, .length3_mm = 5.2, .speed1_kph = 19.98, .speed1_mps = 5.55, .speed2_kph = 10, .speed2_mps = 2.7777777777778, .time1_ms = 4100, .time1_s = 4.1, .time2_ms = 4.1, .time2_s = 0.0041},
    },
    .length1_cm = 520,
    .length1_m = 5.2,
    .length1_mm = 5200,
    .length2_cm = 5.2,
    .length2_m = 0.052,
    .length2_mm = 52,
    .length3_cm = 0.52,
    .length3_m = 0.0052,
    .length3_mm = 5.2,
    .speed1_kph = 19.98,
    .speed1_mps = 5.55,
    .speed2_kph = 10,
    .speed2_mps = 2.7777777777778,
    .time1_ms = 4100,
    .time1_s = 4.1,
    .time2_ms = 4.1,
    .time2_s = 0.0041,
};
