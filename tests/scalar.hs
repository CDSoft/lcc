module Scalar
where
data ConfBoolean = ConfBoolean
    { booleanF' :: Bool
    , booleanT' :: Bool
    }
data ConfFloats = ConfFloats
    { floatsF1' :: Double
    , floatsF2' :: Double
    , floatsF3' :: Double
    , floatsF4' :: Double
    }
data ConfNeg = ConfNeg
    { negA' :: Integer
    , negB' :: Integer
    , negC' :: Integer
    , negD' :: Integer
    }
data ConfPos = ConfPos
    { posA' :: Integer
    , posB' :: Integer
    , posC' :: Integer
    , posD' :: Integer
    , posE' :: Integer
    , posF' :: Integer
    , posG' :: Integer
    , posH' :: Integer
    }
data ConfStrings = ConfStrings
    { stringsS1' :: String
    , stringsS2' :: String
    , stringsS3' :: String
    }
data Conf = Conf
    { a' :: Integer
    , b' :: Integer
    , b1' :: Integer
    , b2' :: Integer
    , b3' :: Integer
    , b4' :: Integer
    , boolean' :: ConfBoolean
    , floats' :: ConfFloats
    , neg' :: ConfNeg
    , pos' :: ConfPos
    , strings' :: ConfStrings
    }
conf :: Conf
conf = Conf
    { a' = 9223372036854775807
    , b' = -9223372036854775808
    , b1' = -9223372036854775807
    , b2' = -9223372036854775806
    , b3' = -9223372036854775805
    , b4' = -9223372036854775804
    , boolean' = ConfBoolean {booleanF' = False, booleanT' = True}
    , floats' = ConfFloats {floatsF1' = 0.0, floatsF2' = 1.0, floatsF3' = 3.1, floatsF4' = 3.1415926535898}
    , neg' = ConfNeg {negA' = -128, negB' = -32768, negC' = -2147483648, negD' = -9223372036854775808}
    , pos' = ConfPos {posA' = 0, posB' = 128, posC' = 256, posD' = 32768, posE' = 65536, posF' = 2147483648, posG' = 4294967296, posH' = 8589934592}
    , strings' = ConfStrings {stringsS1' = "", stringsS2' = "hello", stringsS3' = "hello world!"}
    }
