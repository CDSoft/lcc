#pragma once
#include <stdbool.h>
#include <stdint.h>
#define CONF_S_1_X (1)
#define CONF_S_1_Y ("one")
#define CONF_S_2_X (2)
#define CONF_S_2_Y ("two")
struct t_conf
{
#define CONF_S_SIZE1 2
    struct t_conf_s
    {
        uint8_t x;
#define CONF_S_Y_LEN 4
        char y[CONF_S_Y_LEN];
    } s[CONF_S_SIZE1];
};
extern const struct t_conf CONF;
