#pragma once
#include <stdbool.h>
#include <stdint.h>
#define CONF_FLAT_ARRAY_1 (10)
#define CONF_FLAT_ARRAY_2 (20)
#define CONF_FLAT_ARRAY_3 (30)
#define CONF_FLAT_ARRAY_4 (40)
#define CONF_FLAT_STRUCTURE_FIELD1 (42)
#define CONF_FLAT_STRUCTURE_FIELD2 ("fourty two")
#define CONF_FLAT_STRUCTURE_FIELD3 (true)
#define CONF_FLAT_STRUCTURE_FIELD4 (3.1415926535898)
#define CONF_MEGA_STRUCT_FIELD_01 (42)
#define CONF_MEGA_STRUCT_FIELD_02_X (0)
#define CONF_MEGA_STRUCT_FIELD_02_Y (1)
#define CONF_MEGA_STRUCT_FIELD_03_A ("hi")
#define CONF_MEGA_STRUCT_FIELD_03_B (true)
#define CONF_MEGA_STRUCT_FIELD_04_1 ("one")
#define CONF_MEGA_STRUCT_FIELD_04_2 ("two")
#define CONF_MEGA_STRUCT_FIELD_04_3 ("three")
#define CONF_MEGA_STRUCT_FIELD_05_1_1 (1)
#define CONF_MEGA_STRUCT_FIELD_05_1_2 (2)
#define CONF_MEGA_STRUCT_FIELD_05_2_1 (3)
#define CONF_MEGA_STRUCT_FIELD_05_2_2 (4)
#define CONF_MEGA_STRUCT_FIELD_05_3_1 (5)
#define CONF_MEGA_STRUCT_FIELD_05_3_2 (6)
#define CONF_MEGA_STRUCT_FIELD_06_1_1_1_ST_A (1)
#define CONF_MEGA_STRUCT_FIELD_06_1_1_1_ST_B (2)
#define CONF_MEGA_STRUCT_FIELD_06_1_1_1_X (1)
#define CONF_MEGA_STRUCT_FIELD_06_1_1_1_Y ("one")
#define CONF_MEGA_STRUCT_FIELD_06_1_1_2_X (2)
#define CONF_MEGA_STRUCT_FIELD_06_1_1_2_Y ("two")
#define CONF_MEGA_STRUCT_FIELD_06_1_2_1_X (-1)
#define CONF_MEGA_STRUCT_FIELD_06_1_2_2_Y ("N/A")
#define CONF_MEGA_STRUCT_FIELD_06_1_3_1_ST_A (3)
#define CONF_MEGA_STRUCT_FIELD_06_1_3_1_ST_B (4)
#define CONF_MEGA_STRUCT_FIELD_06_1_3_1_Z (0.1)
#define CONF_MEGA_STRUCT_FIELD_06_1_3_2_Z (0.2)
#define CONF_MEGA_STRUCT_FIELD_06_2_1_1_ARR_1 ("a")
#define CONF_MEGA_STRUCT_FIELD_06_2_1_1_ARR_2 ("b")
#define CONF_MEGA_STRUCT_FIELD_06_2_1_1_X (10)
#define CONF_MEGA_STRUCT_FIELD_06_2_1_1_Y ("ten")
#define CONF_MEGA_STRUCT_FIELD_06_2_1_2_X (20)
#define CONF_MEGA_STRUCT_FIELD_06_2_1_2_Y ("twenty")
#define CONF_MEGA_STRUCT_FIELD_06_2_2_1_X (-2)
#define CONF_MEGA_STRUCT_FIELD_06_2_2_2_Y ("N/A")
#define CONF_MEGA_STRUCT_FIELD_06_2_3_1_ARR_1 ("cd")
#define CONF_MEGA_STRUCT_FIELD_06_2_3_1_ARR_2 ("efg")
#define CONF_MEGA_STRUCT_FIELD_06_2_3_1_Z (3.1415926535898)
#define CONF_MEGA_STRUCT_FIELD_06_2_3_2_Z (1.5707963267949)
#define CONF_MEGA_STRUCT_FIELD_06_3_1_1_S ("")
#define CONF_MEGA_STRUCT_FIELD_06_3_1_1_S2 ("---------------------------------------")
#define CONF_MEGA_STRUCT_FIELD_06_4_1_1_T (true)
#define CONF_SAME_NAME ("global")
#define CONF_TABLE_SAME_NAME ("field")
struct t_conf
{
#define CONF_FLAT_ARRAY_SIZE1 4
    uint8_t flat_array[CONF_FLAT_ARRAY_SIZE1];
    struct t_conf_flat_structure
    {
        uint8_t field1;
#define CONF_FLAT_STRUCTURE_FIELD2_LEN 11
        char field2[CONF_FLAT_STRUCTURE_FIELD2_LEN];
        bool field3;
        double field4;
    } flat_structure;
    struct t_conf_mega_struct
    {
        uint8_t field_01;
        struct t_conf_mega_struct_field_02
        {
            uint8_t x;
            uint8_t y;
        } field_02;
        struct t_conf_mega_struct_field_03
        {
#define CONF_MEGA_STRUCT_FIELD_03_A_LEN 3
            char a[CONF_MEGA_STRUCT_FIELD_03_A_LEN];
            bool b;
        } field_03;
#define CONF_MEGA_STRUCT_FIELD_04_SIZE1 3
#define CONF_MEGA_STRUCT_FIELD_04_LEN 6
        char field_04[CONF_MEGA_STRUCT_FIELD_04_SIZE1][CONF_MEGA_STRUCT_FIELD_04_LEN];
#define CONF_MEGA_STRUCT_FIELD_05_SIZE1 3
#define CONF_MEGA_STRUCT_FIELD_05_SIZE2 2
        uint8_t field_05[CONF_MEGA_STRUCT_FIELD_05_SIZE1][CONF_MEGA_STRUCT_FIELD_05_SIZE2];
#define CONF_MEGA_STRUCT_FIELD_06_SIZE1 4
#define CONF_MEGA_STRUCT_FIELD_06_SIZE2 3
#define CONF_MEGA_STRUCT_FIELD_06_SIZE3 2
        struct t_conf_mega_struct_field_06
        {
#define CONF_MEGA_STRUCT_FIELD_06_ARR_SIZE1 2
#define CONF_MEGA_STRUCT_FIELD_06_ARR_LEN 4
            char arr[CONF_MEGA_STRUCT_FIELD_06_ARR_SIZE1][CONF_MEGA_STRUCT_FIELD_06_ARR_LEN];
#define CONF_MEGA_STRUCT_FIELD_06_S_LEN 1
            char s[CONF_MEGA_STRUCT_FIELD_06_S_LEN];
#define CONF_MEGA_STRUCT_FIELD_06_S2_LEN 40
            char s2[CONF_MEGA_STRUCT_FIELD_06_S2_LEN];
            struct t_conf_mega_struct_field_06_st
            {
                uint8_t a;
                uint8_t b;
            } st;
            bool t;
            int8_t x;
#define CONF_MEGA_STRUCT_FIELD_06_Y_LEN 7
            char y[CONF_MEGA_STRUCT_FIELD_06_Y_LEN];
            double z;
        } field_06[CONF_MEGA_STRUCT_FIELD_06_SIZE1][CONF_MEGA_STRUCT_FIELD_06_SIZE2][CONF_MEGA_STRUCT_FIELD_06_SIZE3];
    } mega_struct;
#define CONF_SAME_NAME_LEN 7
    char same_name[CONF_SAME_NAME_LEN];
    struct t_conf_table
    {
#define CONF_TABLE_SAME_NAME_LEN 6
        char same_name[CONF_TABLE_SAME_NAME_LEN];
    } table;
};
extern const struct t_conf CONF;
