module TemplateTest
where
data ConfS = ConfS
    { sX' :: Integer
    , sY' :: String
    }
data Conf = Conf
    { s' :: [ConfS]
    }
conf :: Conf
conf = Conf
    { s' =
        [ ConfS {sX' = 1, sY' = "one"}
        , ConfS {sX' = 2, sY' = "two"}
        ]
    }
