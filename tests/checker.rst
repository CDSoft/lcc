.. |CONF_BOUNDING_BOX_XMAX| replace:: 12.0

.. |CONF_BOUNDING_BOX_XMIN| replace:: 6.0

.. |CONF_BOUNDING_BOX_YMAX| replace:: 14.0

.. |CONF_BOUNDING_BOX_YMIN| replace:: -2.0

.. |CONF_POINTS_1_X| replace:: 12.0

.. |CONF_POINTS_1_Y| replace:: 7.0

.. |CONF_POINTS_2_X| replace:: 6.0

.. |CONF_POINTS_2_Y| replace:: 14.0

.. |CONF_POINTS_3_X| replace:: 8.0

.. |CONF_POINTS_3_Y| replace:: -2.0

