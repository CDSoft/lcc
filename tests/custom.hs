module Custom
where
data State = INIT | RUNNING | DEAD
data Conf = Conf
    { f' :: String
    , hellostr' :: String
    , initialState' :: State
    , myPtr' :: Int
    , running' :: State
    }
conf :: Conf
conf = Conf
    { f' = "fib"
    , hellostr' = "Hello World!"
    , initialState' = INIT
    , myPtr' = 0x00001234
    , running' = RUNNING
    }
