#include "checker.h"
const struct t_conf CONF = {
    .bounding_box = {.xmax = 12.0, .xmin = 6.0, .ymax = 14.0, .ymin = -2.0},
    .points = {
        [0] = {.x = 12.0, .y = 7.0},
        [1] = {.x = 6.0, .y = 14.0},
        [2] = {.x = 8.0, .y = -2.0},
    },
};
