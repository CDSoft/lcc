#pragma once
#include <stdbool.h>
#include <stdint.h>
#define CONF_HAS_BAR (true)
#define CONF_HAS_BAZ (true)
#define CONF_HAS_FOO (true)
struct t_conf
{
    bool has_bar;
    bool has_baz;
    bool has_foo;
};
extern const struct t_conf CONF;
