module PhysicalUnits
where
data ConfArray = ConfArray
    { arrayAngle1Deg' :: Integer
    , arrayAngle1Rad' :: Double
    , arrayAngle2Deg' :: Double
    , arrayAngle2Rad' :: Double
    , arrayLength1Cm' :: Integer
    , arrayLength1M' :: Double
    , arrayLength1Mm' :: Integer
    , arrayLength2Cm' :: Double
    , arrayLength2M' :: Double
    , arrayLength2Mm' :: Integer
    , arrayLength3Cm' :: Double
    , arrayLength3M' :: Double
    , arrayLength3Mm' :: Double
    , arraySpeed1Kph' :: Double
    , arraySpeed1Mps' :: Double
    , arraySpeed2Kph' :: Integer
    , arraySpeed2Mps' :: Double
    , arrayTime1Ms' :: Integer
    , arrayTime1S' :: Double
    , arrayTime2Ms' :: Double
    , arrayTime2S' :: Double
    }
data Conf = Conf
    { angle1Deg' :: Integer
    , angle1Rad' :: Double
    , angle2Deg' :: Double
    , angle2Rad' :: Double
    , array' :: [ConfArray]
    , length1Cm' :: Integer
    , length1M' :: Double
    , length1Mm' :: Integer
    , length2Cm' :: Double
    , length2M' :: Double
    , length2Mm' :: Integer
    , length3Cm' :: Double
    , length3M' :: Double
    , length3Mm' :: Double
    , speed1Kph' :: Double
    , speed1Mps' :: Double
    , speed2Kph' :: Integer
    , speed2Mps' :: Double
    , time1Ms' :: Integer
    , time1S' :: Double
    , time2Ms' :: Double
    , time2S' :: Double
    }
conf :: Conf
conf = Conf
    { angle1Deg' = 45
    , angle1Rad' = 0.78539816339745
    , angle2Deg' = -60.0
    , angle2Rad' = -1.0471975511966
    , array' =
        [ ConfArray {arrayAngle1Deg' = 45, arrayAngle1Rad' = 0.78539816339745, arrayAngle2Deg' = -60.0, arrayAngle2Rad' = -1.0471975511966, arrayLength1Cm' = 520, arrayLength1M' = 5.2, arrayLength1Mm' = 5200, arrayLength2Cm' = 5.2, arrayLength2M' = 0.052, arrayLength2Mm' = 52, arrayLength3Cm' = 0.52, arrayLength3M' = 0.0052, arrayLength3Mm' = 5.2, arraySpeed1Kph' = 19.98, arraySpeed1Mps' = 5.55, arraySpeed2Kph' = 10, arraySpeed2Mps' = 2.7777777777778, arrayTime1Ms' = 4100, arrayTime1S' = 4.1, arrayTime2Ms' = 4.1, arrayTime2S' = 0.0041}
        ]
    , length1Cm' = 520
    , length1M' = 5.2
    , length1Mm' = 5200
    , length2Cm' = 5.2
    , length2M' = 0.052
    , length2Mm' = 52
    , length3Cm' = 0.52
    , length3M' = 0.0052
    , length3Mm' = 5.2
    , speed1Kph' = 19.98
    , speed1Mps' = 5.55
    , speed2Kph' = 10
    , speed2Mps' = 2.7777777777778
    , time1Ms' = 4100
    , time1S' = 4.1
    , time2Ms' = 4.1
    , time2S' = 0.0041
    }
