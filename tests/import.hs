module Import
where
data Conf = Conf
    { importedParameter' :: Integer
    , parameterFromImportedLib' :: Integer
    }
conf :: Conf
conf = Conf
    { importedParameter' = 42
    , parameterFromImportedLib' = 21
    }
