.. |CONF_FLAT_ARRAY_1| replace:: 10

.. |CONF_FLAT_ARRAY_2| replace:: 20

.. |CONF_FLAT_ARRAY_3| replace:: 30

.. |CONF_FLAT_ARRAY_4| replace:: 40

.. |CONF_FLAT_STRUCTURE_FIELD1| replace:: 42

.. |CONF_FLAT_STRUCTURE_FIELD2| replace:: fourty two

.. |CONF_FLAT_STRUCTURE_FIELD3| replace:: true

.. |CONF_FLAT_STRUCTURE_FIELD4| replace:: 3.1415926535898

.. |CONF_MEGA_STRUCT_FIELD_01| replace:: 42

.. |CONF_MEGA_STRUCT_FIELD_02_X| replace:: 0

.. |CONF_MEGA_STRUCT_FIELD_02_Y| replace:: 1

.. |CONF_MEGA_STRUCT_FIELD_03_A| replace:: hi

.. |CONF_MEGA_STRUCT_FIELD_03_B| replace:: true

.. |CONF_MEGA_STRUCT_FIELD_04_1| replace:: one

.. |CONF_MEGA_STRUCT_FIELD_04_2| replace:: two

.. |CONF_MEGA_STRUCT_FIELD_04_3| replace:: three

.. |CONF_MEGA_STRUCT_FIELD_05_1_1| replace:: 1

.. |CONF_MEGA_STRUCT_FIELD_05_1_2| replace:: 2

.. |CONF_MEGA_STRUCT_FIELD_05_2_1| replace:: 3

.. |CONF_MEGA_STRUCT_FIELD_05_2_2| replace:: 4

.. |CONF_MEGA_STRUCT_FIELD_05_3_1| replace:: 5

.. |CONF_MEGA_STRUCT_FIELD_05_3_2| replace:: 6

.. |CONF_MEGA_STRUCT_FIELD_06_1_1_1_ST_A| replace:: 1

.. |CONF_MEGA_STRUCT_FIELD_06_1_1_1_ST_B| replace:: 2

.. |CONF_MEGA_STRUCT_FIELD_06_1_1_1_X| replace:: 1

.. |CONF_MEGA_STRUCT_FIELD_06_1_1_1_Y| replace:: one

.. |CONF_MEGA_STRUCT_FIELD_06_1_1_2_X| replace:: 2

.. |CONF_MEGA_STRUCT_FIELD_06_1_1_2_Y| replace:: two

.. |CONF_MEGA_STRUCT_FIELD_06_1_2_1_X| replace:: -1

.. |CONF_MEGA_STRUCT_FIELD_06_1_2_2_Y| replace:: N/A

.. |CONF_MEGA_STRUCT_FIELD_06_1_3_1_ST_A| replace:: 3

.. |CONF_MEGA_STRUCT_FIELD_06_1_3_1_ST_B| replace:: 4

.. |CONF_MEGA_STRUCT_FIELD_06_1_3_1_Z| replace:: 0.1

.. |CONF_MEGA_STRUCT_FIELD_06_1_3_2_Z| replace:: 0.2

.. |CONF_MEGA_STRUCT_FIELD_06_2_1_1_ARR_1| replace:: a

.. |CONF_MEGA_STRUCT_FIELD_06_2_1_1_ARR_2| replace:: b

.. |CONF_MEGA_STRUCT_FIELD_06_2_1_1_X| replace:: 10

.. |CONF_MEGA_STRUCT_FIELD_06_2_1_1_Y| replace:: ten

.. |CONF_MEGA_STRUCT_FIELD_06_2_1_2_X| replace:: 20

.. |CONF_MEGA_STRUCT_FIELD_06_2_1_2_Y| replace:: twenty

.. |CONF_MEGA_STRUCT_FIELD_06_2_2_1_X| replace:: -2

.. |CONF_MEGA_STRUCT_FIELD_06_2_2_2_Y| replace:: N/A

.. |CONF_MEGA_STRUCT_FIELD_06_2_3_1_ARR_1| replace:: cd

.. |CONF_MEGA_STRUCT_FIELD_06_2_3_1_ARR_2| replace:: efg

.. |CONF_MEGA_STRUCT_FIELD_06_2_3_1_Z| replace:: 3.1415926535898

.. |CONF_MEGA_STRUCT_FIELD_06_2_3_2_Z| replace:: 1.5707963267949

.. |CONF_MEGA_STRUCT_FIELD_06_3_1_1_S| replace:: 

.. |CONF_MEGA_STRUCT_FIELD_06_3_1_1_S2| replace:: ---------------------------------------

.. |CONF_MEGA_STRUCT_FIELD_06_4_1_1_T| replace:: true

.. |CONF_SAME_NAME| replace:: global

.. |CONF_TABLE_SAME_NAME| replace:: field

