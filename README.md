# Lua Configuration Compiler

lcc (Lua Configuration Compiler) parses a Lua configuration file
and produces source code in various languages.

# Usage

``` sh
$ lcc cfg_1.lua ... cfg_n.lua -o output [-n namespace] -backend_1 ... -backend_n
```

- executes `cfg_1.lua`, ..., `cfg_n.lua`,
- use backends `backend_1`, ..., `backend_n` to convert the Lua global variables produced by the Lua scripts,
- the (optional) `namespace` is prepended to all variable names by the backends,
- the actual output name is determined by replacing the extension of `output` by the backend extension.

``` sh
$ lcc cfg_1.lua ... cfg_n.lua -o output.ext [-n namespace]
```

- does the same as above but the backend is determined by the extension of `output.ext`.

| Options               | Description                                               |
|-----------------------|-----------------------------------------------------------|
| `-h`                  | help                                                      |
| `-o output`           | output name                                               |
| `-n namespace`        | use namespace as the toplevel name in the generated code  |
| `-M file`             | save dependencies in file                                 |
| `-I dir`              | add a directory to the import search directory list       |
| `--cpp-const`         | generate cpp `#define` constants (C backend only)         |

# Configuration script environment

Configuration scripts are executed in a separate Lua environment.
This environment contains standard Lua functions and modules.
It also defines some additional functions.

## `check`

`check` registers checker functions that are run after all scripts are executed.
Checker functions can be used to check some properties and generate new values.

E.g.:

``` lua
    check(function()
        assert(x < 10, "x must be less than 10")    -- check properties
        y = 2*x                                     -- define parameters
    end)
```

## `custom`

`custom` defines a customized scalar type.
It produces a Lua table with the following attributes:

- `__custom`: always `true`
- `name`: name of the custom type

The type definition is a table `{backend={t="type pattern", v="value pattern"}}`
where:

- `backend` is the backend targeted by the custom type
- `t` is the type pattern (`%s` is replaced by the type name)
- `v` is the value pattern (`%s` is replaced by the value, the default pattern is `%s`)

E.g.:

``` lua
    local my_type = custom { c={t="type %s", v="(type)%s"} }
    my_param = my_type(value)
    -- the C type of my_param will be `type my_param`
    -- the C value of my_param will be `my_param = (type)value`
```

## `import`

`import` loads and executes another script in the same environment.
Path are relative to the path of the caller script.

E.g.:

``` lua
    import "path/lib.lua"
```

## `pairs`

`pairs` is redefined to always enumerate keys in the same order.

## `prelude`

`prelude` can add arbitrary text to the generated code.
It takes the backend name and the code to add.

E.g.:

``` lua
    prelude "c" [[
    #include "file.h"
    ]]
    -- This will add `#include "file.h"` to `output.h`
```

## `template` (template backend only)

`template` stores a template to generate a document.
Its argument is a string in which Lua expressions are evaluated and replaced by their values.
Lua expressions are defined by `%{expression}`.

E.g.:

``` lua
    template [[
    LENGTH is %{LENGTH}
    ]]
```

# Type inference

## Scalar types

| Lua type              | Abstract type (field `__type` of the annotated values)    |
|-----------------------|-----------------------------------------------------------|
| `boolean`             | `{kind="bool"}`                                           |
| `number` (integer)    | `{kind="uint" or "int", size ∈ [8, 16, 32, 64]}`          |
| `number` (float)      | `{kind="double"}`                                         |
| `string`              | `{kind="str", size=(length of the string)}`               |

## Custom types

Custom types are `{kind="custom", definitions={backend={t="type pattern", v="value pattern"}}}`.

Custom types are not subject to type inference. The type is fully defined by the user.

## Structures

Structures are Lua tables that contains scalars, structures and arrays.
Each field has its own type.

Hybrid Lua tables are not allowed.

Structures types are `{kind="struct", fields={fieldname=fieldtype}}`.

## Arrays

Arrays are Lua arrays that contains scalars, structures and arrays.
All items have the same type.

The types of items are enlarged (size of integers, size of strings, fields of structures, ...)
so that all items have the same type.

Hybrid Lua tables are not allowed.

Arrays types are `{kind="array", size=(number of items), itemtype=(type of items), dim=(dimension number)}`.

The dimension number is user for multiple dimension arrays.

# Libraries

lcc provides some builtin libraries that can be imported with `import`
or from the command line as a regular Lua script.

## Features

`features.lua` provides the `feature` function that registers a set of feature names
and generates symbols `HAS_XXX` for each feature `XXX`.

## Units

`units.lua` generates new parameters from existing parameters that *look like* physical values with a given dimension.
The dimension is appended to the name of the parameter.
New parameters are (sub)multiple of the original parameters.

| User defined parameters   | New parameters                            |
|---------------------------|-------------------------------------------|
| `X_s`                     | `X_ms = X_s * 1000`                       |
| `X_ms`                    | `X_s = X_ms / 1000`                       |
| `X_m`                     | `X_cm = X_m * 100; X_mm = X_m * 1000`     |
| `X_cm`                    | `X_m = X_cm / 100; X_mm = X_cm * 10`      |
| `X_mm`                    | `X_m = X_mm / 1000; X_cm = X_mm / 10`     |
| `X_deg`                   | `X_rad = X_deg * π/180`                   |
| `X_rad`                   | `X_deg = X_rad * 180/π`                   |
| `X_mps`                   | `X_kph = X_mps * 3.6`                     |
| `X_kph`                   | `X_mps = X_kph / 3.6`                     |

# Backends

## Asymptote: `-asy`

The Asymptote backend produces `output.asy`. It contains Asymptote definitions.

## C: `-c`

The C backend produces `output.h` and `output.c` files:

- `output.h` contains scalar parameters as preprocessor constants
- `output.c` contains scalar and structured parameters as C declarations (types are in `output.h`)

The prelude is added at the beginning of `output.h`.

## Haskell: `-hs`

The Haskell backend produces `output.hs`. It contains Haskell definitions.

## reStructuredText: `-rst`

The reStructuredText backend produces `output.rst`. It contains scalar parameters.
`output.sh` is intended to be sourced by Sphinx as a document prelude.

## Shell: `-sh`

The Shell backend produces `output.sh`. It contains scalar parameters.
`output.sh` is intended to be sourced.

## Template: `-template`

The template backend produces `output` (the extension is not changed).
`output` contains the templates given to the `template` argument
in which Lua expressions are evaluated and replaced by their values.

# Examples and tests

The `tests` directory contains Lua configuration test scripts.
`make tests` will execute these tests and compare the script outputs to the expected outputs.

The syntax of output files are also checked by several tools:

| Language          | Checkers                          |
|-------------------|-----------------------------------|
| Asymptote         | `asy`                             |
| C                 | `clang-tidy`, `clang`, `gcc`      |
| Haskell           | `stack ghc`                       |
| reStructuredText  | `pandoc`                          |
| Shell             | `shellcheck`, `bash`, `zsh`       |
