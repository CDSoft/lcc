--[[ Create new parameters with sub-units
     This script must be added to the command line or imported
     to create parameters for sub-units.
--]]

local function round(x)
    local i = math.floor(x+0.5)
    if math.abs(x - i) < 1e-6 then
        return i
    else
        return x
    end
end

local function set(x, k, v)
    if x[k] == nil then x[k] = v end
end

local function add_units(x)
    for k, v in pairs(x) do
        if type(v) == "table" then add_units(v)
        elseif type(k) == "string" and type(v) == "number" then
            local root, unit = k:match "(.*)_(%w+)$"
            if unit == "s" then
                set(x, root.."_ms", round(v*1000))
            elseif unit == "ms" then
                set(x, root.."_s", round(v/1000))
            elseif unit == "m" then
                set(x, root.."_cm", round(v*100))
                set(x, root.."_mm", round(v*1000))
            elseif unit == "cm" then
                set(x, root.."_m", round(v/100))
                set(x, root.."_mm", round(v*10))
            elseif unit == "mm" then
                set(x, root.."_m", round(v/1000))
                set(x, root.."_cm", round(v/10))
            elseif unit == "deg" then
                set(x, root.."_rad", v * math.pi/180)
            elseif unit == "rad" then
                set(x, root.."_deg", v * 180/math.pi)
            elseif unit == "mps" then
                set(x, root.."_kph", v * 3.6)
            elseif unit == "kph" then
                set(x, root.."_mps", v / 3.6)
            end
        end
    end
end

-- Add a check function that creates new parameters
check(function() add_units(_ENV) end)
