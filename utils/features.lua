--[[ Register a feature list
--]]

function feature(x)

    local function has(name)
        assert(type(name) == "string", "Invalid feature parameter: "..tostring(name))
        _ENV["HAS_"..name] = true
    end

    if type(x) == "table" then
        for i = 1, #x do has(x[i]) end
    else
        has(x)
    end
end
