#!/usr/bin/env lua

print "Lua Configuration Compiler"

local help = [[
usage:
    lcc <cfg.lua ...> -o output [-n namespace] [backend list]
        generates output.ext1 ... output.extn for each backend
    lcc <cfg.lua ...> -o output.ext [-n namespace]
        generates output.ext where ext is a backend

options:
    -h              help
    -o              output name
    -n              namespace
    -M              dependency file
    -I              import directory
    --cpp-const     generate cpp defined constants (C backend only)
]]

local lccpath = arg[0]:gsub("/[^/]*$", "")
local lcclib = lccpath.."/lib"
package.path = lcclib.."/?.lua;"..package.path
local parser = require "parser"

-- Command line arguments

local input_scripts = {}
local output = nil
local backends = {}
local namespace = nil
local depfile = nil
local params = {
    cpp_const = false,
}

parser.add_import_dir(lccpath.."/utils")

while #arg > 0 do
    local a = table.remove(arg, 1)
    local opt = a:match "^-(.*)"
    if opt then
        if a == "-h" then
            print(help)
            os.exit()
        elseif a == "-o" then
            assert(#arg > 0, help)
            assert(not output, "duplicate output parameter")
            output = table.remove(arg, 1)
        elseif a == "-n" then
            assert(#arg > 0, help)
            assert(not namespace, "duplicate namespace parameter")
            namespace = table.remove(arg, 1)
        elseif a == "-M" then
            assert(#arg > 0, help)
            assert(not depfile, "duplicate dependency file parameter")
            depfile = table.remove(arg, 1)
        elseif a == "-I" then
            assert(#arg > 0, help)
            local import_dir = table.remove(arg, 1)
            parser.add_import_dirs(import_dir)
        elseif a == "--cpp-const" then
            params.cpp_const = true
        else
            table.insert(backends, (require("backend/"..opt)))
        end
    else
        table.insert(input_scripts, a)
    end
end

namespace = namespace or "conf"

assert(#input_scripts > 0, "input script not specified")
assert(output, "output not specified")

if #backends == 0 then
    local ext = output:match("%.([^%.]*)$")
    if ext and #ext > 0 then table.insert(backends, require("backend/"..ext)) end
end

assert(#backends > 0, "backends not specified")

print("input:", table.concat(input_scripts, ", "))

-- Execute configuration scripts and collect global definitions

local env = setmetatable({}, parser.envmt)
for _, backend in ipairs(backends) do
    if backend.init then backend.init(env) end
end
for _, input_script in ipairs(input_scripts) do
    parser.import(input_script, env)
end

-- Run the checkers (registered by the configuration scripts)

parser.check()

-- Add type annotations for the backends

local ast = parser.compile(env)

-- Run the backends

local outputs = {}
for _, backend in ipairs(backends) do
    for _, compiled_code in ipairs(backend.compile(output, ast, namespace, params)) do
        local ext = compiled_code[1]
        local code = compiled_code[2]
        local filename = output:gsub("%.[^%.]+$", "").."."..ext
        table.insert(outputs, filename)
        print("output:", filename)
        io.open(filename, "w"):write(code)
    end
end

-- Save dependencies

if depfile then
    parser.save_dependencies(depfile, outputs)
end
